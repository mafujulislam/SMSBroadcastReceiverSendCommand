package com.mafujul.www.smsbroadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import static com.mafujul.www.smsbroadcastreceiver.R.*;

/**
 * Created by Md. Mafujul Islam on 12/1/2015.
 * E-Mail:- mafujuls@gmail.com
 * web:- www.mafujul.com
 */
public class SMSReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Intent Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();

        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {

            Bundle bundle = intent.getExtras();

            SmsMessage[] message = null;
            String body = "";
            String number = "";

            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                message = new SmsMessage[pdus.length];

                for (int i = 0; i < pdus.length; i++) {

                    message[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

                    body += message[i].getMessageBody().toString();
                    number = message[i].getOriginatingAddress();
                }
                Toast.makeText(context, "From: " + number + " \nMessage:\n" + body, Toast.LENGTH_SHORT).show();


                if (body.contains("play")){
                    MediaPlayer objPlayer = new MediaPlayer();
                    objPlayer = MediaPlayer.create(context, raw.pp);
                    objPlayer.start();
                }
            }
        }

    }
}
